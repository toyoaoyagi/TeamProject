var load = function(map, i, url, restaurant){
  i++;
  var content = String(i);
  $.ajax(url, {
    method: 'get',
  }).done(function(response){
    var latlng = response.results[0].geometry.location;
    // var marker = new google.maps.Marker({
    //   position: latlng,
    //   map: map,
    //   title:restaurant[0]
    // });
    var infowindow = new google.maps.InfoWindow({
     content: content,
     position: latlng,
    });
    infowindow.open(map);
  })
}
function initMap() {
  var restaurants = window.iniadluhchmap.restaurants;
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: {lat: 35.781234, lng:  139.715215} // INIAD
  });
  for(var i = 0; i < restaurants.length; i++){
    var restaurant = restaurants[i];
    var url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+restaurant[1]+'&key=AIzaSyDWrg3bSYdtXKS3GKxdxnTxW5-gjTduBjc';
    load(map, i, url, restaurant);
  }
}
