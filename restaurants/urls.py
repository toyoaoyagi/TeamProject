from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^restaurants/([0-9]+)/$', views.detail, name='detail'),
    url(r'^restaurants/create/$', views.create, name='create'),
    url(r'^restaurants/([0-9]+)/update/$', views.update, name='update'),
]
